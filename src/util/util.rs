pub mod number {
    fn is_digit(v: &char) -> bool {
        '0' <= v && v <= '9'
    }
}